# -*- coding: utf-8 -*-
'''A temperature monitor for sTGC electronics connected to an OPC server'''

from typing import Optional
from sys import argv
from os import makedirs
from os.path import join as path_join
import traceback
from opcua import Client
from opcua.ua.uaerrors import BadNodeIdUnknown
from socket import timeout, gaierror # opcua doesn't protect us from this
from concurrent.futures import TimeoutError as CFTimeoutError # nor this
from datetime import datetime
from PyQt5.QtCore import Qt, QObject, QRegularExpression
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QMessageBox
from PyQt5.QtGui import QCloseEvent, QRegularExpressionValidator

from src.opc_temperature_monitor_widget import Ui_Form
from src.detailed_dialog import detailedCritical, detailedWarning
from src.boards import WedgeFebs
from src.feb_vmm_mapper import VmmMapper
from src.monitor import TemperatureMonitor
from src.log import TemperatureLog
from src.plot import OverviewPlot, OverviewPlotInteractive
from src.feb_display import FebDisplay

#TODO: monitor non-FEB temperatures (e.g. L1DDC SCAs)

class QWedgeValidator(QRegularExpressionValidator):
    '''QValidator for a wedge MTF ID'''
    WEDGE_RE = QRegularExpression('20MNIW[LS][AC][CP][0-9]{5}')
    
    def __init__(self, re: Optional[QRegularExpression] = None, 
                 parent: Optional[QObject] = None) -> None:
        if re is None:
            re = self.WEDGE_RE
        super().__init__(re, parent)

class TemperatureMonitorWidget(QWidget, Ui_Form):
    def __init__(self, parent: Optional[QObject] = None, *args, **kwargs) -> None:
        super().__init__(parent, *args, **kwargs)
        self.setupUi(self)
        
        self.vmm_mapping = WedgeFebs().mapping()
        self.monitoring = False
        self.monitor: Optional[TemperatureMonitor] = None
        self.log: Optional[TemperatureLog] = None
        self.plot: Optional[OverviewPlotInteractive] = None
        self.plot_display = QVBoxLayout(self.plot_frame)
        self.feb_guide = FebDisplay()
        self.feb_guide.setWindowTitle('FEB Guide')
        
        self.set_vmm_mapping_b.clicked.connect(self.set_vmm_mapping)
        self.toggle_monitoring_b.clicked.connect(self.toggle_monitoring)
        self.feb_guide_b.clicked.connect(self.open_feb_guide)
        
        #self.wedge_id_entry.setInputMask('')
        self.wedge_id_entry.setValidator(QWedgeValidator())
    
    def set_vmm_mapping(self) -> None:
        vmm_mapper = VmmMapper(self, self.vmm_mapping)
        res = vmm_mapper.exec()
        if res:
            self.vmm_mapping = vmm_mapper.mapping
    
    def toggle_monitoring(self) -> None:
        if self.monitoring:
            self.stop_monitoring()
        else:
            self.start_monitoring()
    
    def start_monitoring(self) -> None:
        self.toggle_monitoring_b.setEnabled(False)
        self.toggle_monitoring_b.setText('Stop monitoring')
        self.monitoring = True
        # clear old objects
        if self.monitor:
            self.monitor = None
        if self.log:
            self.log.close()
            self.log = None
        if self.plot:
            self.plot_display.removeWidget(self.plot)
            self.plot = None
        try:
            # set up objects
            client = Client(self.opc_address_entry.text(), timeout=30_000)
            self.monitor = TemperatureMonitor(self)
            self.monitor.febs = WedgeFebs.from_mapping(self.vmm_mapping)
            if not self.make_logfile():
                self.stop_monitoring()
                return
            plot = OverviewPlot(febs=self.monitor.febs)
            self.plot = OverviewPlotInteractive(self, plot)
            self.plot_display.addWidget(self.plot)
            # connect signals/slots
            self.monitor.monitoring_stopped.connect(self.stop_monitoring)
            self.monitor.monitoring_stopped.connect(self.log.close)
            self.monitor.new_sample.connect(self.log.write_row)
            self.monitor.new_sample.connect(self.plot.update_plot)
            # begin monitoring
            febs = self.monitor.connect(client)
            self.log.write_header(febs)
            self.plot.set_yticklabels_from_febs(febs)
            self.monitor.start_monitoring()
            self.toggle_monitoring_b.setEnabled(True)
        except (ConnectionRefusedError, timeout, gaierror, TimeoutError, CFTimeoutError):
            msg = 'Could not connect to host.'.ljust(80)
            detailedCritical(self, 'Error', msg, '', traceback.format_exc())
            self.stop_monitoring()
        except BadNodeIdUnknown:
            msg = 'OPC server is misconfigured.'.ljust(80)
            detailedCritical(self, 'Error', msg, '', traceback.format_exc())
            self.stop_monitoring()
    
    def make_logfile(self) -> bool:
        wedge_id = self.wedge_id_entry.text()
        if not self.wedge_id_entry.hasAcceptableInput():
            msg = f'The wedge ID "{wedge_id}" does not seem to be valid.'
            msg += '\nContinue anyway?'
            res = QMessageBox.question(self, 'Invalid wedge ID', msg)
            if res != QMessageBox.Yes:
                return False
        wedge_id = self.wedge_id_entry.text()
        filedir = f'./output/{wedge_id}'
        makedirs(filedir, exist_ok=True)
        time = datetime.now().strftime("%Y_%m_%d_%H.%M.%S")
        filename = f'{wedge_id}_{time}.csv'
        filepath = path_join(filedir, filename)
        self.log = TemperatureLog(open(filepath, 'w', newline=''))
        return True
    
    def stop_monitoring(self) -> None:
        self.toggle_monitoring_b.setEnabled(False)
        try:
            self.monitor.monitoring_stopped.disconnect()
        except TypeError:
            # monitor was never connected
            pass
        try:
            self.monitor.stop_monitoring()
        except (gaierror, AttributeError):
            #pyopcua doesn't protect us from the socket library
            msg = 'Error occurred during server disconnect.'.ljust(80)
            detailedWarning(self, 'Warning', msg, '', traceback.format_exc())
        self.monitoring = False
        self.toggle_monitoring_b.setText('Start monitoring')
        self.toggle_monitoring_b.setEnabled(True)
    
    def open_feb_guide(self):
        self.feb_guide.show()
    
    def closeEvent(self, event: QCloseEvent) -> None:
        if self.monitoring:
            msg = 'You are still monitoring, quit anyway?'
            resp = QMessageBox.question(self, 'Quit', msg)
            if resp != QMessageBox.Yes:
                event.ignore()
                return
            else:
                self.stop_monitoring()
        self.feb_guide.deleteLater()
        event.accept()

if __name__ == '__main__':
    def main() -> None:
        app = QApplication(argv)
        monitor = TemperatureMonitorWidget()
        monitor.setAttribute(Qt.WA_DeleteOnClose)
        monitor.show()
        app.exec()
        #TODO: prevent plot from locking during sampling
    
    main()
