# NSW Electronics - FEB and L1DDC temperature monitoring via OPC server

## Table of contents

<!-- 
* [Introduction](#introduction)
* [How to build](#how-to-build)
* [How to run](#how-to-run)
* [User's guide](#users-guide)
-->
[[_TOC_]]

## Introduction

This software is used to monitor the temperature of sTGC wedge electronics, specifically the [Front-End Boards (FEBs)] and [L1DDCs](https://twiki.cern.ch/twiki/bin/view/Atlas/L1DDC). It produces a log file which can be processed by the [Testbench monitor software](https://gitlab.cern.ch/rmcgover/stgc-minidaq-testbench-monitor).

## How to build

Dependencies are listed in the `environment.yml` file, and can be acquired by creating a new [Anaconda](https://www.anaconda.com/) enviroment:
```bash
conda env create -n <env-name> -f environment.yml
conda activate <env-name>
```
or merging with an existing environment:
```bash
conda env update -n <env-name> -f environment.yml
conda activate <env-name>
```

This project is distributed pre-built.

## How to run

```bash
python opc_temperature_monitor.py
```

## User's guide

Enter the OPC server address in "OPC Server ID" (typically, this will start with `opc.tcp://`), and the MTF ID of the wedge being monitored.

Click "Start monitoring" to connect to the server and monitor temperature. Data will be recorded under `output/<wedge ID>/<wedge ID>_<timestamp>.csv`. This log file can be processed with the [Testbench monitor software](https://gitlab.cern.ch/rmcgover/stgc-minidaq-testbench-monitor); use its "Open OPC temperature log" option to do this.

## OPC Server configuration notes

If possible, configure the OPC server to have temperature as a "calculated variable"; the software is smart enough to switch to raw sampling of the ADC if it cannot find a calculated temperature value node, but this method is much slower.

## Planned features

This software is currently in beta. Some features are still in development.

* [x] Add VMM masking beyond the standard (8-VMM sFEBs in Quad 1, 6-VMM sFEBs in Quad 2)
    * [x] Implement VMM mask dialog
    * [x] Implement passing FEBs to `TemperatureMonitor`
    * [x] Implement passing FEBs to `TemperatureLog`
    * [x] Implement passing FEBs to `TemperaturePlot`
    * [x] Ensure missing data can be handled by Testbench Monitor software
* [ ] Place the temperature monitor in its own thread
* [ ] Support monitoring of L1DDCs
    * [x] Implement methods for querying L1DDC temperature
    * [ ] Modify `TemperatureMonitor` to monitor both kinds of boards
        * [ ] Change signature of `new_sample`
    * [ ] Modify `OverviewPlot`
        * [ ] Add column for L1DDC temperatures
        * [ ] Change signature of `munge_sample`
    * [ ] Modify `TemperatureLog`
        * [ ] Change signature of `munge_sample`
        * [ ] Log L1DDC and VMM separately?
    * [ ] Allow user to specify which kinds of boards to monitor
