# -*- coding: utf-8 -*-
'''Provides logging of temperature data to disk'''

from typing import List, TextIO
import csv
from datetime import datetime
from PyQt5.QtCore import pyqtSlot, QObject
from src.boards import WedgeFebs
from src.monitor import Sample

class TemperatureLog(QObject):
    '''CSV writer to log current values
    NB: this class is implemented using csv.writer; therefore, file objects
        should be opened with `newline=''` for maximum compatibility
    '''
    def __init__(self, logfile: TextIO, parent: QObject = None, *args, **kwargs) -> None:
        '''Constructor
        logfile -- the file to log temperatures to
        '''
        super().__init__(parent, *args, **kwargs)
        self.logfile = logfile
        self.writer = csv.writer(self.logfile)
    
    def write_header(self, febs: WedgeFebs) -> None:
        '''Write header to logfile based on `febs`'''
        try:
            vmms = [f'{feb.sca_id}:{feb.name}:VMM{v}' 
                    for feb in febs for v in feb.vmms]
        except AttributeError:
            raise ValueError('FEBS have not been initialized!')
        self.writer.writerow(['time'] + vmms)
    
    @staticmethod
    def munge_sample(sample: Sample) -> List[float]:
        return [temp for temps in sample.values() for temp in temps]
    
    @pyqtSlot(object, object)
    def write_row(self, time: datetime, sample: Sample) -> None:
        self.writer.writerow([time.timestamp()] + self.munge_sample(sample))
    
    @pyqtSlot()
    def close(self) -> None:
        self.logfile.close()
