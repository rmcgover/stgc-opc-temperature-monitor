# -*- coding: utf-8 -*-
'''Provides plotting of temperature data via matplotlib, with interactivity
provided by Qt
'''

from typing import Any, Dict, Optional as Opt, List, Tuple, Type
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.cm import get_cmap
from matplotlib.figure import Figure, GridSpec
import numpy as np
from datetime import datetime
from PyQt5.QtCore import pyqtSlot, QObject
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QSizePolicy

from src.boards import Feb, WedgeFebs
from src.monitor import Sample

class OverviewPlot(FigureCanvas):
    '''Overview plot for a single wedge
    See https://matplotlib.org/examples/user_interfaces/embedding_in_qt5.html
    '''
    
    KINDS = ('PFEB', 'SFEB')
    
    def __init__(self, parent: Opt[QObject] = None, width: float = 10, height: float = 6, dpi: float = 96, febs: Opt[WedgeFebs] = None):
        fig = Figure(figsize=(width, height), dpi=dpi)
        gs = GridSpec(nrows=1, ncols=2, width_ratios=[3, 8])
        self.axs = {'PFEB' : fig.add_subplot(gs[0, 0]), 'SFEB' : fig.add_subplot(gs[0, 1])}
        super().__init__(fig)
        self.setParent(parent)
        self.fig = fig
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        
        if febs is None:
            febs = WedgeFebs()
        self.cm = get_cmap()
        self.cm.set_over(color='r')
        self.cm.set_under(color='r')
        self.cm.set_bad(color='w')
        values = self.munge_sample({f : [0. for _ in f.vmms] for f in febs})
        self.caxs = {}
        self.texts = {kind : [] for kind in self.KINDS}
        for kind in self.KINDS:
            ax = self.axs[kind]
            vals = values[kind]
            self.caxs[kind] = ax.matshow(vals, vmin=15, vmax=50)
            ii, jj = np.meshgrid(*(range(n) for n in reversed(vals.shape)))
            for i, j, t in zip(ii.flatten(), jj.flatten(), vals.flatten()):
                self.texts[kind].append(ax.text(i, j, f'{t:.0f}', va='center', ha='center', color='w'))
            ax.set_title(kind)
            ax.xaxis.tick_bottom()
            ax.xaxis.set_label_text('VMM')
            ax.xaxis.set_label_position('bottom')
            is_kind = lambda feb: feb.is_pfeb if kind == 'P' else feb.is_sfeb
            ax.set_yticks(range(sum(is_kind(feb) for feb in febs)))
            ax.set_yticklabels(feb.name.split('_')[1] for feb in febs if feb.kind == kind)
        self.fig.colorbar(self.caxs['SFEB'])
        self.draw()
    
    def set_yticklabels_from_febs(self, febs: WedgeFebs):
        '''Set the y-tick labels using the SCA IDs from `febs`'''
        for kind in self.KINDS:
            ax = self.axs[kind]
            try:
                ax.set_yticklabels(f'{feb.sca_id} {feb.location}' 
                                   for feb in febs if feb.kind == kind)
            except AttributeError:
                ax.set_yticklabels(feb.location for feb in febs if feb.kind[0] == kind)
    
    @staticmethod
    def munge_sample(sample: Sample) -> Dict[str, np.array]:
        munged = {'PFEB' : [], 'SFEB' : []}
        for feb, temps in sample.items():
            if feb.is_pfeb:
                row = [np.nan] * 3
                for vmm, temp in zip(feb.vmms, temps):
                    row[vmm] = temp
                munged['PFEB'].append(row)
            else:
                row = [np.nan] * 8
                for vmm, temp in zip(feb.vmms, temps):
                    row[vmm] = temp
                munged['SFEB'].append(row)
        if not munged['PFEB']:
            munged['PFEB'] = np.empty((0, 3))
        if not munged['SFEB']:
            munged['SFEB'] = np.empty((0, 8))
        munged = {k : np.array(v) for k, v in munged.items()}
        return munged
    
    @pyqtSlot(object, object)
    def update_plot(self, time: datetime, sample: Dict[Feb, List[float]]) -> None:
        values = self.munge_sample(sample)
        for kind in ('PFEB', 'SFEB'):
            vals = values[kind]
            self.caxs[kind].set_data(vals)
            for text, temp in zip(self.texts[kind], vals.flatten()):
                text.set_text(f'{temp:.0f}')
        self.draw()
    
    def save_figure(self, fname: str, size: Tuple[int, int] = (10, 6), 
                    tight_layout: bool = False, *args, **kwargs) -> None:
        '''Save plot with given `size`; all other args are those of `matplotlib.pyplot.savefig`.
        size         -- tuple of (width, height) in inches
        tight_layout -- whether or not to apply tight_layout before saving
        '''
        old_size = self.fig.get_size_inches()
        self.fig.set_size_inches(*size)
        if tight_layout:
            #NB: tight_layout is not idempotent
            for _ in range(10): self.fig.tight_layout()
        self.fig.savefig(fname, *args, **kwargs)
        self.fig.set_size_inches(*old_size)
        self.draw()

class OverviewPlotInteractive(QWidget):
    '''Wrapper around an MplCanvas to add toolbar for interactivity.
    This wrapper attempts to be transparent, and thus if the user attempts
    to access an attribute this object does not have, it will attempt to 
    access the corresponding attribute of the canvas. (E.g.,
    `<MplCI>.save_figure` is equivalent to `<MplCI>.canvas.save_figure`.)
    For this reason, do be careful with the Qt signals and slots mechanism.
    Members:
    canvas  -- the matplotlib canvas
    toolbar -- the matplotlib toolbar
    '''
    def __init__(self, parent: QObject = None, 
                 canvas: Opt[OverviewPlot] = None, 
                 toolbar_cls: Type[NavigationToolbar] = NavigationToolbar) -> None:
        '''Constructor; takes ownership of canvas.
        parent      -- Qt parent
        canvas      -- MplCanvas instance; this wrapper takes ownership
        toolbar_cls -- NavigationToolbar (or derived) class to instantiate
        '''
        super().__init__(parent=parent)
        layout = QVBoxLayout(self)
        self.canvas = canvas if canvas is not None else OverviewPlot()
        self.canvas.setParent(self)
        layout.addWidget(self.canvas)
        self.toolbar = toolbar_cls(self.canvas, self)
        layout.addWidget(self.toolbar)
    
    # make this widget transparent w/r/t the canvas
    def __getattr__(self, name: str) -> Any:
        return getattr(self.canvas, name)        
