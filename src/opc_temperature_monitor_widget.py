# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'src/opc_temperature_monitor_widget.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1000, 600)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.opc_address_entry = QtWidgets.QLineEdit(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.opc_address_entry.sizePolicy().hasHeightForWidth())
        self.opc_address_entry.setSizePolicy(sizePolicy)
        self.opc_address_entry.setMinimumSize(QtCore.QSize(250, 0))
        self.opc_address_entry.setObjectName("opc_address_entry")
        self.horizontalLayout.addWidget(self.opc_address_entry)
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.wedge_id_entry = QtWidgets.QLineEdit(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wedge_id_entry.sizePolicy().hasHeightForWidth())
        self.wedge_id_entry.setSizePolicy(sizePolicy)
        self.wedge_id_entry.setMinimumSize(QtCore.QSize(150, 0))
        self.wedge_id_entry.setObjectName("wedge_id_entry")
        self.horizontalLayout.addWidget(self.wedge_id_entry)
        self.set_vmm_mapping_b = QtWidgets.QPushButton(Form)
        self.set_vmm_mapping_b.setObjectName("set_vmm_mapping_b")
        self.horizontalLayout.addWidget(self.set_vmm_mapping_b)
        self.toggle_monitoring_b = QtWidgets.QPushButton(Form)
        self.toggle_monitoring_b.setObjectName("toggle_monitoring_b")
        self.horizontalLayout.addWidget(self.toggle_monitoring_b)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.feb_guide_b = QtWidgets.QPushButton(Form)
        self.feb_guide_b.setObjectName("feb_guide_b")
        self.horizontalLayout.addWidget(self.feb_guide_b)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.plot_frame = QtWidgets.QFrame(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plot_frame.sizePolicy().hasHeightForWidth())
        self.plot_frame.setSizePolicy(sizePolicy)
        self.plot_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.plot_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.plot_frame.setObjectName("plot_frame")
        self.verticalLayout.addWidget(self.plot_frame)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "OPC UA sTGC FEB Temperature Monitor"))
        self.label_2.setText(_translate("Form", "OPC Server ID:"))
        self.opc_address_entry.setText(_translate("Form", "opc.tcp://um-felix1.cern.ch:48020"))
        self.label.setText(_translate("Form", "Wedge MTF ID:"))
        self.wedge_id_entry.setText(_translate("Form", "20MNIWXXX#####"))
        self.set_vmm_mapping_b.setText(_translate("Form", "Set VMM mapping"))
        self.toggle_monitoring_b.setText(_translate("Form", "Start monitoring"))
        self.feb_guide_b.setText(_translate("Form", "FEB guide"))
