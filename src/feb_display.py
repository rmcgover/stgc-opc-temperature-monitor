# -*- coding: utf-8 -*-
'''Widgets to display an overview map of where the VMMs and other ASICs are
on the pFEB and sFEB
'''

from PyQt5.QtCore import Qt
from PyQt5.QtSvg import QGraphicsSvgItem
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView

FEB_DISPLAY = 'src/feb_display.svg'

class AspectedGraphicsView(QGraphicsView):
    '''QGraphicsView that maintains the aspect ratio of its scene'''
    
    def paintEvent(self, event):
        scene = self.scene()
        if scene:
            self.fitInView(scene.sceneRect(), Qt.KeepAspectRatio)
        super().paintEvent(event)

class FebDisplay(AspectedGraphicsView):
    '''QWidget that shows ASIC locations on a pFEB and sFEB'''
    
    def __init__(self, parent=None):
        '''Constructor
        Arguments:
            parent -- the Qt parent
        '''
        scene = QGraphicsScene(parent)
        febs = QGraphicsSvgItem(FEB_DISPLAY)
        scene.addItem(febs)
        super().__init__(scene, parent)

if __name__ == '__main__':
    from sys import argv
    from PyQt5.QtWidgets import QApplication
    
    def main():
        app = QApplication(argv)
        v = FebDisplay()
        v.setAttribute(Qt.WA_DeleteOnClose)
        v.show()
        app.exec()
    
    main()
