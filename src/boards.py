# -*- coding: utf-8 -*-
'''Provides data classes for sTGC boards, as well as smart collections of them
and OPC server integration
'''

from __future__ import annotations
from typing import Any, Iterable, List, Optional, Tuple, Union
from itertools import product
from opcua import Client
from opcua.common.node import Node
import numpy as np

### Board implementation

class _BoardKind(str):
    '''ABC to implement something like an enum of string values'''
    
    _KINDS = ()
    
    def __new__(cls, *args, **kwargs) -> _BoardKind:
        new = super().__new__(cls, *args, **kwargs)
        try:
            new.__index = cls._KINDS.index(args[0])
        except ValueError:
            raise ValueError(args[0])
        return new
    
    @classmethod
    def all(cls) -> Tuple[_BoardKind]:
        return tuple(cls(k) for k in cls._KINDS)
    
    def __index__(self) -> int:
        return self.__index
    
    def __eq__(self, other) -> bool:
        try:
            return self.__index == int(other)
        except ValueError:
            return super().__eq__(other)

class Feb:
    '''Data class for a FEB'''
    
    class FebKind(_BoardKind):
        '''Data class for a FEB kind (eases indexing)'''
        
        _KINDS = ('PFEB', 'SFE8', 'SFE6')
        
        def __eq__(self, other) -> bool:
            if str(other) == 'SFEB':
                return self in ('SFE8', 'SFE6')
            return super().__eq__(other)
    
    KINDS = FebKind.all()
    
    def __init__(self, kind: str, layer: int, quad: int) -> None:
        # default mask
        if kind == 'SFEB':
            kind = 'SFE8' if quad == 1 else 'SFE6'
        self.kind = self.FebKind(kind)
        self.layer = layer
        self.quad = quad
        self._vmms: Optional[Iterable[int]] = None
    
    def __str__(self) -> str:
        return self.name
    
    def __repr__(self) -> str:
        return f'{self.__class__.__name__}{self.kind, self.layer, self.quad}'
    
    @property
    def name(self) -> str:
        '''The FEB name'''
        kindchar = self.kind[0]
        if kindchar == 'P':
            kind = 'PFEB'
        else:
            kind = 'SFEB' + self.kind[-1]
        return f'{kind}_{self.location}'
    
    @property
    def location(self) -> str:
        '''The FEB location'''
        return f'L{self.layer}Q{self.quad}'
    
    @property
    def is_pfeb(self) -> bool:
        '''Whether or not the FEB is a pFEB'''
        return self.kind[0] == 'P'
    
    @property
    def is_sfeb(self) -> bool:
        '''Whether or not the FEB is an sFEB'''
        return self.kind[0] == 'S'
    
    @property
    def vmms(self) -> Iterable[int]:
        '''The VMM numbers of the FEB'''
        if self._vmms is not None:
            return self._vmms
        if self.kind == 'PFEB':
            return range(3)
        elif self.kind == 'SFE6':
            return range(8)[2:]
        elif self.kind == 'SFE8':
            return range(8)
        else:
            raise ValueError(self.kind)
    
    @vmms.setter
    def vmms(self, vmms: Iterable[int]):
        '''Set the VMM mask'''
        self._vmms = vmms
    
    @property
    def feb_nodename(self) -> str:
        '''The OPC UA FEB node name'''
        return f'ns=2;s={self.name}'
    
    @property
    def sca_id_nodename(self) -> str:
        '''The OPC UA SCA ID node name'''
        return f'{self.feb_nodename}.id'
    
    @property
    def vmmpdo_nodenames(self) -> List[str]:
        '''The OPC UA PDO node names of the VMMs'''
        return [f'{self.feb_nodename}.ai.vmmPdo{v}' for v in self.vmms]

class L1ddc:
    '''Data class for an L1DDC'''
    
    class L1ddcKind(_BoardKind):
        _KINDS = ('P', 'S')
    KINDS = L1ddcKind.all()
    
    def __init__(self, number: int) -> None:
        self.number = number
        self.kind = self.KINDS[self.number % 2]
        self.layer = (self.number + 1) // 2
    
    def __str__(self) -> str:
        return self.name
    
    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.number})'
    
    @property
    def name(self) -> str:
        '''The L1DDC name'''
        return f'L1DDC{self.number}_L{self.layer}{self.kind}'
    
    @property
    def l1ddc_nodename(self) -> str:
        '''The OPC UA L1DDC node name'''
        return f'ns=2;s={self.name}'
    
    @property
    def sca_id_nodename(self) -> str:
        '''The OPC UA SCA ID node name'''
        return f'{self.l1ddc_nodename}.id'
    
    @property
    def gbtx_temperature_nodenames(self) -> List[str]:
        '''The OPC UA node names of the GBTX temperatures'''
        return [f'{self.l1ddc_nodename}.ai.GBTX{g}_TEMP.temperature' for g in (1, 2)]

### Board collections with OPC integration

def slice_to_range(s: Any, min_i: int, max_i: int) -> Union[range, tuple]:
    '''If s is a slice, return a range from min_i to max_i sliced by s.
    Otherwise, return a one-element tuple containing s
    '''
    if isinstance(s, slice):
        return range(min_i, max_i + 1)[s]
    else:
        return (s,)

class WedgeFebs:
    '''Iterable container for all the FEBs of a wedge'''
    
    def __init__(self, client: Optional[Client] = None) -> None:
        '''Initialize wedge FEBs, and fetch extra attributes if `client` is
        provided (otherwise, use `fetch_attrs`)
        '''
        self.febs = [Feb(k, l, q) for k in ('PFEB', 'SFEB')
                     for l in range(1, 4 + 1) 
                     for q in range(1, 3 + 1)]
        if client is not None:
            self.fetch_attrs(client)
    
    def fetch_attrs(self, client: Client) -> None:
        '''Fetch attributes from `client` to add to the FEBs:
            sca_id : the SCA ID of the FEB
            pdo_nodes : the VMM PDO nodes from the OPC UA server
        '''
        self._fetch_sca_ids(client)
        self._fetch_vmmpdo_nodes(client)
    
    def _fetch_sca_ids(self, client: Client) -> None:
        for feb in self.febs:
            feb.sca_id = client.get_node(feb.sca_id_nodename).get_value()
    
    def _fetch_vmmpdo_nodes(self, client: Client) -> None:
        for feb in self.febs:
            feb.vmmpdo_nodes = [client.get_node(name) for name in feb.vmmpdo_nodenames]
    
    MappingRow = Tuple[Tuple[Feb.FebKind, int, int], bool, Tuple[bool, ...]]
    Mapping = Tuple[MappingRow, ...]
    
    def mapping(self) -> WedgeFebs.Mapping:
        '''Return a non-ragged reduced mapping of the wedge. Each element is
        a 3-tuple:
            1. The FEB mount point (3-tuple of kind, layer, quad)
            2. Whether or not there is a FEB mounted there
            3. 8-tuple of whether or not a VMM exists with that index
        '''
        result = ()
        for k, l, q in product(('PFEB', 'SFEB'), range(1, 4 + 1), range(1, 3 + 1)):
            try:
                feb = self[(k, l, q)]
                args = (feb.kind, feb.layer, feb.quad)
                enabled = True
                vmms = tuple(vmm in feb.vmms for vmm in range(8))
            except KeyError:
                if k == 'SFEB':
                    k = Feb.FebKind('SFE8' if q == 1 else 'SFE6')
                args = (k, l, q)
                enabled = False
                vmms = (False,) * 8
            result += ((args, enabled, vmms),)
        return result
    
    @classmethod
    def from_mapping(cls, mapping: WedgeFebs.Mapping) -> WedgeFebs:
        '''Create a `WedgeFebs` from a `WedgeFebs.Mapping`'''
        result = cls()
        result.febs = []
        for args, enabled, vmms in mapping:
            if not enabled:
                continue
            feb = Feb(*args)
            feb.vmms = [vmm for vmm, en in enumerate(vmms) if en]
            result.febs.append(feb)
        return result
    
    def __str__(self) -> str:
        return str(self.febs)
    
    def __iter__(self) -> List[Feb]:
        return iter(self.febs)
    
    def __getitem__(self, key: Union[int, slice, tuple]) -> Union[Feb, List[Feb]]:
        if isinstance(key, tuple):
            k, l, q = key
            if any(isinstance(v, slice) for v in key):
                k = slice_to_range(k, 0, len(Feb.KINDS))
                l = slice_to_range(l, 1, 4 + 1)
                q = slice_to_range(q, 1, 3 + 1)
                return [feb for feb in self.febs
                        if feb.kind in k and feb.layer in l and feb.quad in q]
            else:
                for feb in self.febs:
                    if k == feb.kind and l == feb.layer and q == feb.quad:
                        return feb
                else:
                    raise KeyError(key)
        else:
            return self.febs[key]

class WedgeL1ddcs:
    '''Iterable container for all the L1DDCs of a wedge'''
    
    def __init__(self, client: Optional[Client] = None) -> None:
        '''Initialize wedge L1DDCs, and fetch extra attributes if `client` is
        provided (otherwise, use `fetch_attrs`)
        '''
        self.l1ddcs = [L1ddc(i) for i in range(1, 8 + 1)]
        if client is not None:
            self.fetch_attrs(client)
    
    def fetch_attrs(self, client: Client) -> None:
        '''Members fetched from client:
            sca_id : the SCA ID of the L1DDC
            temperature_nodes : the temperature nodes from the OPC UA server
        '''
        self._fetch_sca_ids(client)
        self._fetch_gbtx_temperature_nodes(client)
    
    def _fetch_sca_ids(self, client: Client) -> None:
        for l1ddc in self.l1ddcs:
            l1ddc.sca_id = client.get_node(l1ddc.sca_id_nodename).get_value()
    
    def _fetch_gbtx_temperature_nodes(self, client: Client) -> None:
        for l1ddc in self.l1ddcs:
            l1ddc.temperature_nodes = [client.get_node(name) for name in l1ddc.gbtx_temperature_nodenames]
    
    def __str__(self) -> str:
        return str(self.l1ddcs)
    
    def __iter__(self) -> List[L1ddc]:
        return iter(self.l1ddcs)
    
    def __getitem__(self, key: Union[int, slice, tuple]) -> Union[L1ddc, List[L1ddc]]:
        if isinstance(key, tuple):
            try:
                k, l = key
                if any(isinstance(v, slice) for v in key):
                    k = slice_to_range(k, 0, len(L1ddc.KINDS))
                    l = slice_to_range(l, 1, 4 + 1)
                    return [l1ddc for l1ddc in self.l1ddcs
                            if l1ddc.kind in k and l1ddc.layer in l]
                else:
                    for l1ddc in self.l1ddcs:
                        if k == l1ddc.kind and l == l1ddc.layer:
                            return l1ddc
                    else:
                        raise KeyError(key)
            except ValueError:
                n, = key
                if isinstance(n, slice):
                    start = n.start - 1 if n.start is not None else None
                    stop = n.stop - 1 if n.stop is not None else None
                    n = slice(start, stop, n.step)
                    return self.l1ddcs[n]
                else:
                    return self.l1ddcs[n-1]
        return self.l1ddcs[key]

def vmm_adc_to_temperature(adc: float) -> float:
    '''Convert a 12-bit adc value to its corresponding value in mV,
    then convert that level to a temperature value using the formula in
    the VMM3a specification (§8, "Cooling").
    '''
    return (725 - adc * (1000 / (2 ** 12))) / 1.85

def vmm_temperature_to_adc(temperature: float) -> float:
    '''Compute the (float) inverse of adc_to_temperature.
    NB: rounding is left as an exercise for the reader.
    '''
    return (725 - 1.85 * temperature) / (1000 / (2 ** 12))

def vmm_temperature_from_raw_adc(pdo_node: Node, nsamples: int = 1) -> float:
    '''Get the temperature by converting the PDO (slow)'''
    meth = pdo_node.get_child('2:getConsecutiveRawSamples')
    if nsamples > 1:
        pdo = np.mean(pdo_node.call_method(meth, nsamples))
    else:
        pdo, = pdo_node.call_method(meth, 1)
    return vmm_adc_to_temperature(pdo)

def vmm_temperature_from_calculated(pdo_node: Node) -> float:
    '''Get the temperature from the calculated variable (fast)'''
    return pdo_node.get_child('2:temperature').get_value()

def gbtx_temperature_from_calculated(temperature_node: Node) -> float:
    '''Get the temperatuer from the calculated variable (fast)'''
    return temperature_node.get_value()
