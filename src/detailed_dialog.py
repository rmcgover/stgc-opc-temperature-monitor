# -*- coding: utf-8 -*-
'''Convenience methods to emulate QMessageBox static functions with informative and detailed text.
'''

from PyQt5.QtWidgets import QMessageBox

def detailedDialog(parent, icon, title, 
                   text, informativetext, detailedtext,
                   buttons, defaultbutton):
    '''Open a QMessageBox with the given parameters in front of the specified parent widget.'''
    m = QMessageBox(parent)
    m.setIcon(icon)
    m.setWindowTitle(title)
    m.setText(text)
    m.setInformativeText(informativetext)
    m.setDetailedText(detailedtext)
    m.setStandardButtons(buttons)
    m.setDefaultButton(defaultbutton)
    return m.exec_()

def detailedInformation(parent, title, text, informativetext, detailedtext,
                        buttons=QMessageBox.Ok, 
                        defaultbutton=QMessageBox.NoButton):
    '''Open an information message box with the given parameters in front of the specified parent widget.'''
    return detailedDialog(parent, QMessageBox.Information, 
                          title, text, informativetext, detailedtext,
                          buttons, defaultbutton)

def detailedWarning(parent, title, text, informativetext, detailedtext,
                    buttons=QMessageBox.Ok, 
                    defaultbutton=QMessageBox.NoButton):
    '''Open a warning message box with the given parameters in front of the specified parent widget.'''
    return detailedDialog(parent, QMessageBox.Warning, 
                          title, text, informativetext, detailedtext,
                          buttons, defaultbutton)
    
def detailedCritical(parent, title, text, informativetext, detailedtext,
                     buttons=QMessageBox.Ok, 
                     defaultbutton=QMessageBox.NoButton):
    '''Open a critical message box with the given parameters in front of the specified parent widget.'''
    return detailedDialog(parent, QMessageBox.Critical, 
                          title, text, informativetext, detailedtext,
                          buttons, defaultbutton)

def detailedQuestion(parent, title, text, informativetext, detailedtext,
                     buttons=(QMessageBox.Yes | QMessageBox.No),
                     defaultbutton=QMessageBox.NoButton):
    '''Open a question message box with the given parameters in front of the specified parent widget.'''
    return detailedDialog(parent, QMessageBox.Question,
                          title, text, informativetext, detailedtext,
                          buttons, defaultbutton)
    
