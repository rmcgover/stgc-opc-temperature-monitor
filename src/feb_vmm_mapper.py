# -*- coding: utf-8 -*-

from typing import List, Optional, Tuple, Union
from dataclasses import dataclass

from PyQt5.QtCore import pyqtSlot, QObject
from PyQt5.QtWidgets import QWidget, QDialog, QVBoxLayout, QGridLayout, QLabel, QComboBox, QCheckBox, QButtonGroup, QDialogButtonBox as QDBB

from src.boards import WedgeFebs, Feb

_FebKindIndex = Union[int, Feb.FebKind]

class FebKindComboBox(QComboBox):
    '''QComboBox for all values of Feb.KINDS'''
    
    def __init__(self, parent: Optional[QWidget] = None, index: Optional[_FebKindIndex] = None) -> None:
        super().__init__(parent)
        self.addItems(Feb.KINDS)
        if index is not None:
            self.setCurrentIndex(index)
    
    def setCurrentIndex(self, index: _FebKindIndex) -> None:
        if isinstance(index, Feb.FebKind):
            self.setCurrentIndex(Feb.KINDS.index(index))
            return
        super().setCurrentIndex(index)

class VmmGroup(QButtonGroup):
    '''QButtonGroup to manage checkboxes for enabling VMMs'''
    
    _ENABLE = {'PFEB' : (1, 1, 1, 0, 0, 0, 0, 0),
               'SFE6' : (0, 0, 1, 1, 1, 1, 1, 1),
               'SFE8' : (1, 1, 1, 1, 1, 1, 1, 1)}
    
    def __init__(self, parent: Optional[QWidget] = None) -> None:
        super().__init__(parent)
        self.setExclusive(False)
        self._buttons = tuple(QCheckBox(parent) for i in range(8))
        for i, button in enumerate(self._buttons):
            self.addButton(button, i)
        self.addButton = None
    
    def __iter__(self) -> Tuple[QCheckBox]:
        return iter(self._buttons)
    
    @pyqtSlot(bool)
    def setVisible(self, show: bool) -> None:
        '''Show or hide all buttons in this group'''
        for button in self._buttons:
            button.setVisible(show)
    
    @pyqtSlot(int)
    def set_enabled_from_kind(self, index: _FebKindIndex) -> None:
        '''Enable or disable buttons in this group based on a FebKind'''
        kind = str(Feb.KINDS[index]) if isinstance(index, int) else str(index)
        for button, enable in zip(self._buttons, self._ENABLE[kind]):
            if enable and not button.isEnabled():
                button.setChecked(True)
            button.setEnabled(enable)
            if not enable:
                button.setChecked(False)

@dataclass
class _MappingRow:
    '''Helper dataclass to align GUI with WedgeFebs.Mapping'''
    kind: Feb.FebKind
    layer: int
    quad: int
    enable: QCheckBox
    vmms: QButtonGroup

class VmmMapper(QDialog):
    '''Dialog to get a VMM mapping. Use the result of `exec` to determine if
    the `mapping` member variable is available.
    '''
    
    def __init__(self, parent: Optional[QWidget] = None, mapping: WedgeFebs.Mapping = None) -> None:
        super().__init__(parent)
        self.mapping = WedgeFebs().mapping() if mapping is None else mapping
        
        self.setWindowTitle('VMM Mapping')
        self.layout = QVBoxLayout(self)
        
        self.mapper_layout = QGridLayout()
        self.layout.addLayout(self.mapper_layout)
        
        header = ['Layer', 'Quad', 'Kind', 'Enable'] + [f'VMM{i}' for i in range(8)]
        for j, title in enumerate(header):
            self.mapper_layout.addWidget(QLabel(title, self), 0, j)
        
        self._mapping = []
        for i, (args, en, vmms) in enumerate(self.mapping):
            kind, layer, quad = args
            # FEB position
            self.mapper_layout.addWidget(QLabel(f'L{layer}', self), i + 1, 0)
            self.mapper_layout.addWidget(QLabel(f'Q{quad}', self), i + 1, 1)
            # FEB kind
            combobox = FebKindComboBox(self, kind)
            self.mapper_layout.addWidget(combobox, i + 1, 2)
            # FEB mask
            feb_enable = QCheckBox(self)
            feb_enable.setChecked(en)
            self.mapper_layout.addWidget(feb_enable, i + 1, 3)
            # FEB VMM mask
            group = VmmGroup(self)
            for j, button in enumerate(group):
                self.mapper_layout.addWidget(button, i + 1, j + 4)
            for vmm, en_vmm in enumerate(vmms):
                group.button(vmm).setChecked(en_vmm)
            group.set_enabled_from_kind(kind)
            combobox.currentIndexChanged.connect(group.set_enabled_from_kind)
            group.setVisible(en)
            feb_enable.toggled.connect(group.setVisible)
            # Mapping row
            self._mapping.append(_MappingRow(kind, layer, quad, feb_enable, group))
        
        self.dialog_buttons = QDBB(QDBB.Cancel | QDBB.Ok, self)
        self.dialog_buttons.accepted.connect(self.accept)
        self.dialog_buttons.rejected.connect(self.reject)
        self.layout.addWidget(self.dialog_buttons)
    
    def accept(self) -> None:
        '''Commit the mapping to the `mapping` member variable'''
        mapping = []
        for row in self._mapping:
            args = [row.kind, row.layer, row.quad]
            en = row.enable.isChecked()
            vmms = [button.isChecked() for button in row.vmms]
            mapping.append([args, en, vmms])
        self.mapping = mapping
        super().accept()

if __name__ == '__main__':
    from sys import argv
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QApplication
    
    def main():
        app = QApplication(argv)
        
        m = VmmMapper(None)
        m.setAttribute(Qt.WA_DeleteOnClose)
        
        m.show()
        app.exec()
    
    main()
