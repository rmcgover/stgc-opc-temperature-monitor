# -*- coding: utf-8 -*-
'''Provides the temperature monitor'''

from typing import Dict, List, Optional
import traceback
from datetime import datetime
from opcua import Client
from opcua.ua.uaerrors import BadNoMatch, BadOutOfService
from concurrent.futures import TimeoutError as CFTimeoutError
from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from src.detailed_dialog import detailedWarning, detailedCritical
from src.boards import Feb, WedgeFebs, vmm_temperature_from_calculated, vmm_temperature_from_raw_adc

Sample = Dict[Feb, List[float]]

#TODO: add 'Raises:' to docstrings
class TemperatureMonitor(QObject):
    '''Monitors temperature using an OPC UA client'''
    #TODO: move to own thread
    def __init__(self, parent: QObject = None, *args, **kwargs) -> None:
        super().__init__(parent, *args, **kwargs)
        self.timer = QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.setInterval(5000) #FIXME: magic numbers are bad
        self.timer.timeout.connect(self.sample_temperature)
        
        self.client: Optional[Client] = None
        self.febs: Optional[WedgeFebs] = None
        self.method: Optional[str] = None
    
    def connect(self, client: Client) -> WedgeFebs:
        '''Connect `client` to the OPC UA server. To specify a custom set of
        FEBs to monitor, assign the `febs` attribute before calling `connect`.
        '''
        if self.client is not None:
            raise RuntimeError('Client already connected')
        self.client = client
        self.client.connect()
        self.client.load_type_definitions()
        if self.febs is None:
            self.febs = WedgeFebs()
        self.febs.fetch_attrs(client)
        return self.febs
    
    def start_monitoring(self, method: str = None) -> None:
        '''Start monitoring'''
        if self.client is None:
            raise RuntimeError('Client not connected')
        if method is None:
            self._get_sample_method()
        else:
            self.method = method
        self.sample_temperature()
    
    def _get_sample_method(self) -> None:
        pdo_node = self.febs[0].vmmpdo_nodes[0]
        try:
            vmm_temperature_from_calculated(pdo_node)
            self.method = 'calculated'
        except BadNoMatch:
            self.method = 'raw'
    
    def sample_temperature(self) -> None:
        '''Sample temperature
        EMITS: new_sample: datetime, Sample
        '''
        time = datetime.now()
        response = {}
        try:
            for feb in self.febs:
                response[feb] = []
                for pdo_node in feb.vmmpdo_nodes:
                    if self.method == 'calculated':
                        temp = vmm_temperature_from_calculated(pdo_node)
                    else:
                        temp = vmm_temperature_from_raw_adc(pdo_node)
                    response[feb].append(temp)
        except (BadOutOfService, CFTimeoutError):
            msg = 'Error during monitoring.'.ljust(80)
            detailedCritical(None, 'Error', msg, '', traceback.format_exc()) #FIXME
            self.stop_monitoring()
            return
        self.new_sample.emit(time, response)
        self.timer.start()
    
    new_sample = pyqtSignal((object, object))
    
    def stop_monitoring(self) -> None:
        '''Stop monitoring
        EMITS: monitoring_stopped
        '''
        if self.client is None:
            return
        self.timer.timeout.disconnect()
        self.timer.stop()
        try:
            self.client.disconnect()
        except CFTimeoutError:
            msg = 'Error occurred during server disconnect.'.ljust(80)
            detailedWarning(None, 'Warning', msg, '', traceback.format_exc()) #FIXME
        self.client = None
        self.monitoring_stopped.emit()
    
    monitoring_stopped = pyqtSignal()
